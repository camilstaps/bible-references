implementation module Bible.Parse

/**
 * This file is part of bible-references.
 *
 * Bible-references is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * Bible-references is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with bible-references. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Control.Applicative
import Control.Monad
import Data.Either
import Data.Error
import Data.Func
import Data.Functor
import Data.List
import qualified Data.Map
import Data.Maybe
from Text import class Text(toLowerCase), instance Text String, concat3
import Text.Parsers.Simple.Chars
import Text.Parsers.Simple.Core

import Bible

parseReference :: !String -> MaybeError String Reference
parseReference s = case parse parser [c \\ c <-: s] of
	Left [e:_] -> Error e
	Right ref -> Ok ref

(@?) infixr 4 :: !(Parser t a) !String -> Parser t a
(@?) p err = Parser f
where
	f input = case runParser p input of
		([], []) -> runParser (pError err) input
		res -> res

parser :: Parser Char Reference
parser =
	(book @? "Expected book name") >>= \book ->
	bookChapterSeparator *>
	case book of
		Obadia ->
			((\verse -> {book=book, chapter=1, verse=verse}) <$> verse) <*
			(pPeek >>= \stream -> case stream of
				[c:_:_] | isMember c chapterVerseSeparators ->
					pError "Do not specify the chapter for Obadiah"
				_ ->
					end)
		_ ->
			liftM2
				(\chapter verse -> {book=book, chapter=chapter, verse=verse})
				(chapter <* chapterVerseSeparator)
				(verse <* end)
where
	chapter =
		toInt <$> toString <$> (pSome pDigit @! "No chapter and verse specified") >>= \n
			| n == 0 -> pError "Chapter must be positive"
			| otherwise -> pure n
	verse =
		toInt <$> toString <$> (pSome pDigit @! "No verse specified") >>= \n
			| n == 0 -> pError "Verse must be positive"
			| otherwise -> pure n

	end =
		pPeek >>= \stream -> case stream of
			[] -> pure ()
			_ -> pError "Spurious content after reference"

book :: Parser Char Book
book =
	toString <$> liftM2
		(\mbPrefix words -> maybe [] (\d -> [d,' ']) mbPrefix ++ words)
		(optional (pDigit <* optional pSpace)) // 1/2 Samuel etc.
		(flatten <$> pSepBy1 (pSome pAlpha) (pSome pSpace)) >>= \book ->
	case getAbbreviation book of
		?Just book -> pure book
		?None -> pError (concat3 "Unknown book '" book "'")

bookChapterSeparator :: Parser Char Char
bookChapterSeparator =
		pSpace
	<<|>
		(pPeek >>= \input -> case input of
			[] -> pError "No chapter and verse specified"
			[c:_] -> pError (concat3 "'" {c} "' is not a valid book-chapter separator"))

chapterVerseSeparator :: Parser Char Char
chapterVerseSeparator =
		pSatisfy (\c -> isMember c chapterVerseSeparators)
	<<|>
		(pPeek >>= \input -> case input of
			[] -> pError "No verse specified"
			[c:_] -> pError (concat3 "'" {c} "' is not a valid chapter-verse separator"))

chapterVerseSeparators :: [Char]
chapterVerseSeparators =: [':.,']

getAbbreviation :: String -> ?Book
getAbbreviation s = 'Data.Map'.get (toLowerCase s) abbreviations

abbreviations :: 'Data.Map'.Map String Book
abbreviations =:
	'Data.Map'.fromList $
	concatMap (\(book,abbrs) -> [(toLowerCase a, book) \\ a <- abbrs]) $ // Swap; lowercase for consistency; flatten
	map (\(book,abbrs) -> (book, [englishName book:abbrs])) $ // Add the full name
	// See: https://www.logos.com/bible-book-abbreviations (some obscure ones are not copied)
	[ (Genesis, ["Gen", "Ge", "Gn"])
	, (Exodus, ["Ex", "Exod", "Exo"])
	, (Leviticus, ["Lev", "Le", "Lv"])
	, (Numeri, ["Num", "Nu", "Nm", "Nb"])
	, (Deuteronomium, ["Deut", "De", "Dt"])

	, (Josua, ["Josh", "Jos", "Jsh"])
	, (Judices, ["Judg", "Jdg", "Jg", "Jdgs"])
	, (Samuel_I, ["1 Sam", "1 Sm", "1 Sa", "1 S", "I Sam", "I Sa", "1Sam", "1Sm", "1Sa", "1S"])
	, (Samuel_II, ["2 Sam", "2 Sm", "2 Sa", "2 S", "II Sam", "II Sa", "2Sam", "2Sm", "2Sa", "2S"])
	, (Reges_I, ["1 Kgs", "1 Ki", "1 K", "I Kgs", "I Ki", "1Kgs", "1Ki", "1K"])
	, (Reges_II, ["2 Kgs", "2 Ki", "2 K", "II Kgs", "II Ki", "2Kgs", "2Ki", "2K"])
	, (Jesaia, ["Isa", "Is"])
	, (Jeremia, ["Jer", "Je", "Jr"])
	, (Ezechiel, ["Ezek", "Eze", "Ezk"])
	, (Hosea, ["Hos", "Ho"])
	, (Joel, ["Jo"])
	, (Amos, ["Am"])
	, (Obadia, ["Obad", "Ob"])
	, (Jona, ["Jon", "Jnh"])
	, (Micha, ["Mic", "Mc"])
	, (Nahum, ["Nah", "Na"])
	, (Habakuk, ["Hab", "Hb"])
	, (Zephania, ["Zeph", "Zep", "Zp"])
	, (Haggai, ["Hag", "Hg"])
	, (Sacharia, ["Zech", "Zec", "Zc"])
	, (Maleachi, ["Mal", "Ml"])

	, (Psalmi, ["Ps", "Psa", "Psm", "Pss"])
	, (Iob, ["Jb"])
	, (Proverbia, ["Prov", "Pro", "Prv", "Pr"])
	, (Ruth, ["Ru", "Rth"])
	, (Canticum, ["Song", "So", "Cant"])
	, (Ecclesiastes, ["Eccles", "Eccl", "Ecc", "Ec", "Qoh"])
	, (Threni, ["Lam", "La"])
	, (Esther, ["Est", "Esth", "Es"])
	, (Daniel, ["Dan", "Da", "Dn"])
	, (Esra, ["Ezr", "Ez"])
	, (Nehemia, ["Neh", "Ne"])
	, (Chronica_I, ["1 Chron", "1 Chr", "1 Ch", "1Chron", "1Chr", "1Ch", "I Chron", "I Chr", "I Ch"])
	, (Chronica_II, ["2 Chron", "2 Chr", "2 Ch", "2Chron", "2Chr", "2Ch", "II Chron", "II Chr", "II Ch"])
	]
