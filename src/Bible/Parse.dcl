definition module Bible.Parse

/**
 * This file is part of bible-references.
 *
 * Bible-references is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * Bible-references is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with bible-references. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.Error import :: MaybeError

from Bible import :: Reference

/**
 * Try to parse a human-readable biblical reference.
 */
parseReference :: !String -> MaybeError String Reference
