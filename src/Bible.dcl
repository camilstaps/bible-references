definition module Bible

/**
 * This file is part of bible-references.
 *
 * Bible-references is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * Bible-references is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with bible-references. If not, see <https://www.gnu.org/licenses/>.
 */

from StdOverloaded import class fromString, class toString, class ==

//* This follows the BHSA Latin names
:: Book
	= Genesis
	| Exodus
	| Leviticus
	| Numeri
	| Deuteronomium

	| Josua
	| Judices
	| Samuel_I
	| Samuel_II
	| Reges_I
	| Reges_II
	| Jesaia
	| Jeremia
	| Ezechiel
	| Hosea
	| Joel
	| Amos
	| Obadia
	| Jona
	| Micha
	| Nahum
	| Habakuk
	| Zephania
	| Haggai
	| Sacharia
	| Maleachi

	| Psalmi
	| Iob
	| Proverbia
	| Ruth
	| Canticum
	| Ecclesiastes
	| Threni
	| Esther
	| Daniel
	| Esra
	| Nehemia
	| Chronica_I
	| Chronica_II

	| UnknownBook !String

instance == Book
instance fromString Book
instance toString Book
fromEnglishName :: !String -> Book
englishName :: !Book -> String

:: Reference =
	{ book    :: !Book
	, chapter :: !Int
	, verse   :: !Int
	}

instance == Reference
instance toString Reference

hebrew_to_english :: !Reference -> [Reference]
