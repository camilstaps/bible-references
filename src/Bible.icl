implementation module Bible

/**
 * This file is part of bible-references.
 *
 * Bible-references is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * Bible-references is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with bible-references. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

instance == Book
where
	(==) a b = case a of
		Genesis       -> b=:Genesis
		Exodus        -> b=:Exodus
		Leviticus     -> b=:Leviticus
		Numeri        -> b=:Numeri
		Deuteronomium -> b=:Deuteronomium

		Josua         -> b=:Josua
		Judices       -> b=:Judices
		Samuel_I      -> b=:Samuel_I
		Samuel_II     -> b=:Samuel_II
		Reges_I       -> b=:Reges_I
		Reges_II      -> b=:Reges_II
		Jesaia        -> b=:Jesaia
		Jeremia       -> b=:Jeremia
		Ezechiel      -> b=:Ezechiel
		Hosea         -> b=:Hosea
		Joel          -> b=:Joel
		Amos          -> b=:Amos
		Obadia        -> b=:Obadia
		Jona          -> b=:Jona
		Micha         -> b=:Micha
		Nahum         -> b=:Nahum
		Habakuk       -> b=:Habakuk
		Zephania      -> b=:Zephania
		Haggai        -> b=:Haggai
		Sacharia      -> b=:Sacharia
		Maleachi      -> b=:Maleachi

		Psalmi        -> b=:Psalmi
		Iob           -> b=:Iob
		Proverbia     -> b=:Proverbia
		Ruth          -> b=:Ruth
		Canticum      -> b=:Canticum
		Ecclesiastes  -> b=:Ecclesiastes
		Threni        -> b=:Threni
		Esther        -> b=:Esther
		Daniel        -> b=:Daniel
		Esra          -> b=:Esra
		Nehemia       -> b=:Nehemia
		Chronica_I    -> b=:Chronica_I
		Chronica_II   -> b=:Chronica_II

		UnknownBook a -> case b of UnknownBook b -> a==b; _ -> False

instance fromString Book
where
	fromString book = case book of
		"Genesis"       -> Genesis
		"Exodus"        -> Exodus
		"Leviticus"     -> Leviticus
		"Numeri"        -> Numeri
		"Deuteronomium" -> Deuteronomium

		"Josua"         -> Josua
		"Judices"       -> Judices
		"Samuel_I"      -> Samuel_I
		"Samuel_II"     -> Samuel_II
		"Reges_I"       -> Reges_I
		"Reges_II"      -> Reges_II
		"Jesaia"        -> Jesaia
		"Jeremia"       -> Jeremia
		"Ezechiel"      -> Ezechiel
		"Hosea"         -> Hosea
		"Joel"          -> Joel
		"Amos"          -> Amos
		"Obadia"        -> Obadia
		"Jona"          -> Jona
		"Micha"         -> Micha
		"Nahum"         -> Nahum
		"Habakuk"       -> Habakuk
		"Zephania"      -> Zephania
		"Haggai"        -> Haggai
		"Sacharia"      -> Sacharia
		"Maleachi"      -> Maleachi

		"Psalmi"        -> Psalmi
		"Iob"           -> Iob
		"Proverbia"     -> Proverbia
		"Ruth"          -> Ruth
		"Canticum"      -> Canticum
		"Ecclesiastes"  -> Ecclesiastes
		"Threni"        -> Threni
		"Esther"        -> Esther
		"Daniel"        -> Daniel
		"Esra"          -> Esra
		"Nehemia"       -> Nehemia
		"Chronica_I"    -> Chronica_I
		"Chronica_II"   -> Chronica_II

		b               -> UnknownBook b

instance toString Book
where
	toString book = case book of
		Genesis       -> "Genesis"
		Exodus        -> "Exodus"
		Leviticus     -> "Leviticus"
		Numeri        -> "Numeri"
		Deuteronomium -> "Deuteronomium"

		Josua         -> "Josua"
		Judices       -> "Judices"
		Samuel_I      -> "Samuel_I"
		Samuel_II     -> "Samuel_II"
		Reges_I       -> "Reges_I"
		Reges_II      -> "Reges_II"
		Jesaia        -> "Jesaia"
		Jeremia       -> "Jeremia"
		Ezechiel      -> "Ezechiel"
		Hosea         -> "Hosea"
		Joel          -> "Joel"
		Amos          -> "Amos"
		Obadia        -> "Obadia"
		Jona          -> "Jona"
		Micha         -> "Micha"
		Nahum         -> "Nahum"
		Habakuk       -> "Habakuk"
		Zephania      -> "Zephania"
		Haggai        -> "Haggai"
		Sacharia      -> "Sacharia"
		Maleachi      -> "Maleachi"

		Psalmi        -> "Psalmi"
		Iob           -> "Iob"
		Proverbia     -> "Proverbia"
		Ruth          -> "Ruth"
		Canticum      -> "Canticum"
		Ecclesiastes  -> "Ecclesiastes"
		Threni        -> "Threni"
		Esther        -> "Esther"
		Daniel        -> "Daniel"
		Esra          -> "Esra"
		Nehemia       -> "Nehemia"
		Chronica_I    -> "Chronica_I"
		Chronica_II   -> "Chronica_II"

		UnknownBook b -> b

englishName :: !Book -> String
englishName book = case book of
	Genesis       -> "Genesis"
	Exodus        -> "Exodus"
	Leviticus     -> "Leviticus"
	Numeri        -> "Numbers"
	Deuteronomium -> "Deuteronomy"

	Josua         -> "Joshua"
	Judices       -> "Judges"
	Samuel_I      -> "1 Samuel"
	Samuel_II     -> "2 Samuel"
	Reges_I       -> "1 Kings"
	Reges_II      -> "2 Kings"
	Jesaia        -> "Isaiah"
	Jeremia       -> "Jeremiah"
	Ezechiel      -> "Ezekiel"
	Hosea         -> "Hosea"
	Joel          -> "Joel"
	Amos          -> "Amos"
	Obadia        -> "Obadiah"
	Jona          -> "Jonah"
	Micha         -> "Micah"
	Nahum         -> "Nahum"
	Habakuk       -> "Habakuk"
	Zephania      -> "Zephaniah"
	Haggai        -> "Haggai"
	Sacharia      -> "Zechariah"
	Maleachi      -> "Malachi"

	Psalmi        -> "Psalms"
	Iob           -> "Job"
	Proverbia     -> "Proverbs"
	Ruth          -> "Ruth"
	Canticum      -> "Song of Songs"
	Ecclesiastes  -> "Ecclesiastes"
	Threni        -> "Lamentations"
	Esther        -> "Esther"
	Daniel        -> "Daniel"
	Esra          -> "Ezra"
	Nehemia       -> "Nehemiah"
	Chronica_I    -> "1 Chronicles"
	Chronica_II   -> "2 Chronicles"

	UnknownBook b -> b

fromEnglishName :: !String -> Book
fromEnglishName book = case book of
	"Genesis"       -> Genesis
	"Exodus"        -> Exodus
	"Leviticus"     -> Leviticus
	"Numbers"       -> Numeri
	"Deuteronomy"   -> Deuteronomium

	"Joshua"        -> Josua
	"Judges"        -> Judices
	"1 Samuel"      -> Samuel_I
	"2 Samuel"      -> Samuel_II
	"1 Kings"       -> Reges_I
	"2 Kings"       -> Reges_II
	"Isaiah"        -> Jesaia
	"Jeremiah"      -> Jeremia
	"Ezekiel"       -> Ezechiel
	"Hosea"         -> Hosea
	"Joel"          -> Joel
	"Amos"          -> Amos
	"Obadiah"       -> Obadia
	"Jonah"         -> Jona
	"Micah"         -> Micha
	"Nahum"         -> Nahum
	"Habakuk"       -> Habakuk
	"Zephaniah"     -> Zephania
	"Haggai"        -> Haggai
	"Zechariah"     -> Sacharia
	"Malachi"       -> Maleachi

	"Psalms"        -> Psalmi
	"Job"           -> Iob
	"Proverbs"      -> Proverbia
	"Ruth"          -> Ruth
	"Song of Songs" -> Canticum
	"Ecclesiastes"  -> Ecclesiastes
	"Lamentations"  -> Threni
	"Esther"        -> Esther
	"Daniel"        -> Daniel
	"Ezra"          -> Esra
	"Nehemiah"      -> Nehemia
	"1 Chronicles"  -> Chronica_I
	"2 Chronicles"  -> Chronica_II

	b -> UnknownBook b

instance toString Reference
where
	toString {book,chapter,verse} =
		englishName book +++ " " +++ toString chapter +++ ":" +++ toString verse

instance == Reference
where
	(==) a b = a.book == b.book && a.chapter == b.chapter && a.verse == b.verse

hebrew_to_english :: !Reference -> [Reference]
hebrew_to_english r=:{book=b,chapter=c,verse=v} = case b of
	// This is from the SBL Handbook of Style, 2nd ed, appendix B
	Genesis
		| c==32 &&          v== 1 -> [{r & chapter=31, verse=55}]
		| c==32 &&          v<=33 -> [{r & verse=v-1}]
	Exodus
		| c== 7 && 26<=v && v<=29 -> [{r & chapter=8, verse=v-25}]
		| c== 8 &&          v<=28 -> [{r & verse=v+4}]
		| c==21 &&          v==37 -> [{r & chapter=22, verse=1}]
		| c==22 &&          v<=30 -> [{r & verse=v+1}]
	Leviticus
		| c== 5 && 20<=v && v<=26 -> [{r & chapter=6, verse=v-19}]
		| c== 6 &&          v<=23 -> [{r & verse=v+7}]
	Numeri
		| c==17 &&          v<=15 -> [{r & chapter=16, verse=v+35}]
		| c==17 && 16<=v && v<=28 -> [{r & verse=v-15}]
		| c==25 &&          v==19 -> [r, {r & chapter=26, verse=1}]
		| c==30 &&          v== 1 -> [{r & chapter=29, verse=40}]
		| c==30 &&  2<=v && v<=17 -> [{r & verse=v-1}]
	Deuteronomium
		| c==13 &&          v== 1 -> [{r & chapter=12, verse=32}]
		| c==13 &&  2<=v && v<=19 -> [{r & verse=v-1}]
		| c==23 &&          v== 1 -> [{r & chapter=22, verse=30}]
		| c==23 &&  2<=v && v<=26 -> [{r & verse=v-1}]
		| c==28 &&          v==69 -> [{r & chapter=29, verse=1}]
		| c==29 &&          v<=28 -> [{r & verse=v+1}]
	Samuel_I
		| c==21 &&          v== 1 -> [{r & chapter=20, verse=42}, r]
		| c==21 &&  2<=v && v<=16 -> [{r & verse=v-1}]
		| c==24 &&          v== 1 -> [{r & chapter=23, verse=29}]
		| c==24 &&  2<=v && v<=23 -> [{r & verse=v-1}]
	Samuel_II
		| c==19 &&          v== 1 -> [{r & chapter=18, verse=33}]
		| c==19 &&  2<=v && v<=44 -> [{r & verse=v-1}]
	Reges_I
		| c== 5 &&  1<=v && v<=14 -> [{r & chapter=4, verse=v+20}]
		| c== 5 && 15<=v && v<=32 -> [{r & verse=v-14}]
		| c==18 &&          v==34 -> [{r & verse=33}, r]
		| c==20 &&          v== 3 -> [{r & verse=2}, r]
		| c==22 &&          v==21 -> [r, {r & verse=22}]
		| c==22 &&          v==44 -> [{r & verse=43}, r]
		| c==22 && 45<=v && v<=54 -> [{r & verse=v-1}]
	Reges_II
		| c==12 &&          v== 1 -> [{r & chapter=11, verse=21}]
		| c==12 &&  2<=v && v<=22 -> [{r & verse=v-1}]
	Chronica_I
		| c== 5 && 27<=v && v<=41 -> [{r & chapter=6, verse=v-26}]
		| c== 6 &&          v<=66 -> [{r & verse=v+15}]
		| c==12 &&          v== 5 -> [{r & verse=4}]
		| c==12 &&  6<=v && v<=41 -> [{r & verse=v-1}]
	Chronica_II
		| c== 1 &&          v==18 -> [{r & chapter=2, verse=1}]
		| c== 2 &&          v<=17 -> [{r & verse=v+1}]
		| c==13 &&          v==23 -> [{r & chapter=14, verse=1}]
		| c==14 &&          v<=14 -> [{r & verse=v+1}]
	Nehemia
		| c== 3 && 33<=v && v<=38 -> [{r & chapter=4, verse=v-32}]
		| c== 4 &&          v<=17 -> [{r & verse=v+6}]
		| c==10 &&          v== 1 -> [{r & chapter=9, verse=38}]
		| c==10 &&  2<=v && v<=40 -> [{r & verse=v-1}]
	Iob
		| c==40 && 25<=v && v<=32 -> [{r & chapter=41, verse=v-24}]
		| c==41 &&          v<=26 -> [{r & verse=v+8}]
	Psalmi
		| c== 3 &&  2<=v && v<= 9 -> [{r & verse=v-1}]
		| c== 4 &&  2<=v && v<= 9 -> [{r & verse=v-1}]
		| c== 5 &&  2<=v && v<=13 -> [{r & verse=v-1}]
		| c== 6 &&  2<=v && v<=11 -> [{r & verse=v-1}]
		| c== 7 &&  2<=v && v<=18 -> [{r & verse=v-1}]
		| c== 8 &&  2<=v && v<=10 -> [{r & verse=v-1}]
		| c== 9 &&  2<=v && v<=21 -> [{r & verse=v-1}]
		| c==12 &&  2<=v && v<= 9 -> [{r & verse=v-1}]
		| c==13 &&  2<=v && v<= 5 -> [{r & verse=v-1}]
		| c==13 &&          v== 6 -> [{r & verse=5}, r]
		| c==18 &&  2<=v && v<=51 -> [{r & verse=v-1}]
		| c==19 &&  2<=v && v<=15 -> [{r & verse=v-1}]
		| c==20 &&  2<=v && v<=10 -> [{r & verse=v-1}]
		| c==21 &&  2<=v && v<=14 -> [{r & verse=v-1}]
		| c==22 &&  2<=v && v<=32 -> [{r & verse=v-1}]
		| c==30 &&  2<=v && v<=13 -> [{r & verse=v-1}]
		| c==31 &&  2<=v && v<=25 -> [{r & verse=v-1}]
		| c==34 &&  2<=v && v<=23 -> [{r & verse=v-1}]
		| c==36 &&  2<=v && v<=13 -> [{r & verse=v-1}]
		| c==38 &&  2<=v && v<=23 -> [{r & verse=v-1}]
		| c==39 &&  2<=v && v<=14 -> [{r & verse=v-1}]
		| c==40 &&  2<=v && v<=18 -> [{r & verse=v-1}]
		| c==41 &&  2<=v && v<=14 -> [{r & verse=v-1}]
		| c==42 &&  2<=v && v<=12 -> [{r & verse=v-1}]
		| c==44 &&  2<=v && v<=27 -> [{r & verse=v-1}]
		| c==45 &&  2<=v && v<=18 -> [{r & verse=v-1}]
		| c==46 &&  2<=v && v<=12 -> [{r & verse=v-1}]
		| c==47 &&  2<=v && v<=10 -> [{r & verse=v-1}]
		| c==48 &&  2<=v && v<=15 -> [{r & verse=v-1}]
		| c==49 &&  2<=v && v<=21 -> [{r & verse=v-1}]
		| c==51 &&          v== 2 -> [{r & verse=1}]
		| c==51 &&  3<=v && v<=21 -> [{r & verse=v-2}]
		| c==52 &&          v== 2 -> [{r & verse=1}]
		| c==52 &&  3<=v && v<=11 -> [{r & verse=v-2}]
		| c==53 &&  2<=v && v<= 7 -> [{r & verse=v-1}]
		| c==54 &&          v== 2 -> [{r & verse=1}]
		| c==54 &&  3<=v && v<= 9 -> [{r & verse=v-2}]
		| c==55 &&  2<=v && v<=24 -> [{r & verse=v-1}]
		| c==56 &&  2<=v && v<=14 -> [{r & verse=v-1}]
		| c==57 &&  2<=v && v<=12 -> [{r & verse=v-1}]
		| c==58 &&  2<=v && v<=12 -> [{r & verse=v-1}]
		| c==59 &&  2<=v && v<=18 -> [{r & verse=v-1}]
		| c==60 &&          v== 2 -> [{r & verse=1}]
		| c==60 &&  3<=v && v<=14 -> [{r & verse=v-2}]
		| c==61 &&  2<=v && v<= 9 -> [{r & verse=v-1}]
		| c==62 &&  2<=v && v<=13 -> [{r & verse=v-1}]
		| c==63 &&  2<=v && v<=12 -> [{r & verse=v-1}]
		| c==64 &&  2<=v && v<=11 -> [{r & verse=v-1}]
		| c==65 &&  2<=v && v<=14 -> [{r & verse=v-1}]
		| c==67 &&  2<=v && v<= 8 -> [{r & verse=v-1}]
		| c==68 &&  2<=v && v<=36 -> [{r & verse=v-1}]
		| c==69 &&  2<=v && v<=37 -> [{r & verse=v-1}]
		| c==70 &&  2<=v && v<= 6 -> [{r & verse=v-1}]
		| c==75 &&  2<=v && v<=11 -> [{r & verse=v-1}]
		| c==76 &&  2<=v && v<=13 -> [{r & verse=v-1}]
		| c==77 &&  2<=v && v<=21 -> [{r & verse=v-1}]
		| c==80 &&  2<=v && v<=20 -> [{r & verse=v-1}]
		| c==81 &&  2<=v && v<=17 -> [{r & verse=v-1}]
		| c==83 &&  2<=v && v<=19 -> [{r & verse=v-1}]
		| c==84 &&  2<=v && v<=13 -> [{r & verse=v-1}]
		| c==85 &&  2<=v && v<=14 -> [{r & verse=v-1}]
		| c==88 &&  2<=v && v<=19 -> [{r & verse=v-1}]
		| c==89 &&  2<=v && v<=53 -> [{r & verse=v-1}]
		| c==92 &&  2<=v && v<=16 -> [{r & verse=v-1}]
		| c==102 && 2<=v && v<=29 -> [{r & verse=v-1}]
		| c==108 && 2<=v && v<=14 -> [{r & verse=v-1}]
		| c==140 && 2<=v && v<=14 -> [{r & verse=v-1}]
		| c==142 && 2<=v && v<= 8 -> [{r & verse=v-1}]
	Ecclesiastes
		| c== 4 &&          v==17 -> [{r & chapter=5, verse=1}]
		| c== 5 &&          v<=19 -> [{r & verse=v+1}]
	Canticum
		| c== 7 &&          v== 1 -> [{r & chapter=6, verse=13}]
		| c== 7 &&  2<=v && v<=14 -> [{r & verse=v-1}]
	Jesaia
		| c== 8 &&          v==23 -> [{r & chapter=9, verse=1}]
		| c== 9 &&          v<=20 -> [{r & verse=v+1}]
		| c==63 &&          v==19 -> [r, {r & chapter=64, verse=1}]
		| c==64 &&          v<=11 -> [{r & verse=v+1}]
	Jeremia
		| c== 8 &&          v==23 -> [{r & chapter=9, verse=1}]
		| c== 9 &&          v<=25 -> [{r & verse=v+1}]
	Ezechiel
		| c==21 &&          v<= 5 -> [{r & chapter=20, verse=v+44}]
		| c==21 &&  6<=v && v<=37 -> [{r & verse=v-5}]
	Daniel
		| c== 3 && 31<=v && v<=33 -> [{r & chapter=4, verse=v-30}]
		| c== 4 &&          v<=34 -> [{r & verse=v+3}]
		| c== 6 &&          v== 1 -> [{r & chapter=5, verse=31}]
		| c== 6 &&  2<=v && v<=29 -> [{r & verse=v-1}]
	Hosea
		| c== 2 &&          v<= 2 -> [{r & chapter=1, verse=v+9}]
		| c== 2 &&  3<=v && v<=25 -> [{r & verse=v-2}]
		| c==12 &&          v== 1 -> [{r & chapter=11, verse=12}]
		| c==12 &&  2<=v && v<=15 -> [{r & verse=v-1}]
		| c==14 &&          v== 1 -> [{r & chapter=13, verse=16}]
		| c==14 &&  2<=v && v<=10 -> [{r & verse=v-1}]
	Joel
		| c== 3 &&          v<= 5 -> [{r & chapter=2, verse=v+27}]
		| c== 4 &&          v<=21 -> [{r & chapter=3}]
	Jona
		| c== 2 &&          v== 1 -> [{r & chapter=1, verse=17}]
		| c== 2 &&  2<=v && v<=11 -> [{r & verse=v-1}]
	Micha
		| c== 4 &&          v==14 -> [{r & chapter=5, verse=1}]
		| c== 5 &&          v<=14 -> [{r & verse=v+1}]
	Nahum
		| c== 2 &&          v== 1 -> [{r & chapter=1, verse=15}]
		| c== 2 &&  2<=v && v<=14 -> [{r & verse=v-1}]
	Sacharia
		| c== 2 &&          v<= 4 -> [{r & chapter=1, verse=v+17}]
		| c== 2 &&  5<=v && v<=17 -> [{r & verse=v-4}]
	Maleachi
		| c== 3 && 19<=v && v<=24 -> [{r & chapter=4, verse=v-18}]
	_
		-> [r]
