module parsetest

import Data.Error
import Data.Error.GenPrint
import Gast
import Gast.CommandLine
import StdEnv
import Text

import Bible
import Bible.Parse

Start w = exposeProperties [OutputTestEvents] [] properties w
where
	properties =
		[ testSuccess "Genesis 1:1" (Genesis, 1, 1)
		, testSuccess "Exod 12,34" (Exodus, 12, 34)

		, testSuccess "1 Sam 10.9" (Samuel_I, 10, 9)
		, testFailure "Samuel 10:9" "Unknown book 'Samuel'"
		, testFailure "3 Sam 10:9" "Unknown book '3 Sam'"

		, testSuccess "Ob 12" (Obadia, 1, 12)
		, testFailure "Ob 1:1" "Do not specify the chapter for Obadiah"

		, testFailure "Gen 0:1" "Chapter must be positive"
		, testFailure "Gen 1:0" "Verse must be positive"
		, testFailure "Gen 0:0" "Chapter must be positive"

		, testFailure "Gen 1 1" "' ' is not a valid chapter-verse separator"
		, testFailure "Gen.1:1" "'.' is not a valid book-chapter separator"

		, testFailure "Gen 1:1." "Spurious content after reference"
		, testFailure "Ob 1." "Spurious content after reference"

		, testFailure "Genesis" "No chapter and verse specified"
		, testFailure "Genesis " "No chapter and verse specified"
		, testFailure "Genesis 1" "No verse specified"
		, testFailure "Genesis 1:" "No verse specified"

		, testFailure "" "Expected book name"

		, name "no generic error message" noGenericErrorMessage
		]

derive genShow MaybeError, Reference, Book
derive gPrint Reference, Book

testSuccess :: !String !(!Book, !Int, !Int) -> Property
testSuccess sref (book, chapter, verse) =
	name
		(concat4 "'" sref "' => " (toString ref))
		(parseReference sref =.= Ok ref)
where
	ref = {book=book, chapter=chapter, verse=verse}

testFailure :: !String !String -> Property
testFailure sref err =
	name
		(concat4 "'" sref "' => " err)
		(parseReference sref =.= Error err)

noGenericErrorMessage :: !String -> Property
noGenericErrorMessage sref =
	parseReference sref <.> Error "No parse results with a fully consumed input."
