# Bible-references

A library to manipulate references to the Hebrew Bible in [Clean][].

## Author & License
This library is written, maintained, and copyright &copy; by [Camil Staps][].

This project is licensed under AGPL v3; see the [LICENSE](/LICENSE) file.

[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
