# Changelog

### v1.1

- Chore: update to base 2.0.

#### v1.0.1

- Chore: move to https://clean-lang.org.

## v1.0.0

First tagged version.
